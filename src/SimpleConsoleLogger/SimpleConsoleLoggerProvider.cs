﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace SimpleConsoleLogger
{
    public class SimpleConsoleLoggerProvider : ILoggerProvider
    {
        private readonly Func<string, LogLevel, bool> _filter;

        public SimpleConsoleLoggerProvider(Func<string, LogLevel, bool> filter)
        {
            _filter = filter;
        }
        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new SimpleConsoleLogger(categoryName, _filter);
        }
    }
}
