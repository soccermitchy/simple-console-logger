﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace SimpleConsoleLogger
{
    public class LogLevelInfo
    {
        public ConsoleColor consoleColor;
        public string prefix;
    }
    public class SimpleConsoleLogger : ILogger
    {
        private string _categoryName;
        private string _parsedCategoryName;
        private Func<string, LogLevel, bool> _filter;

        private Dictionary<LogLevel, LogLevelInfo> _dataMapping = new Dictionary<LogLevel, LogLevelInfo>()
        {
            {LogLevel.Trace, new LogLevelInfo() {consoleColor = ConsoleColor.DarkGray, prefix = "TRACE"}},    // [TRACE]
            {LogLevel.Debug, new LogLevelInfo() {consoleColor = ConsoleColor.Gray, prefix = "DBG  "}},        // [DBG  ]
            {LogLevel.Information, new LogLevelInfo() {consoleColor = ConsoleColor.White, prefix = "INFO "}}, // [INFO ]
            {LogLevel.Warning, new LogLevelInfo() {consoleColor = ConsoleColor.Yellow, prefix = "WARN "}},    // [WARN ]
            {LogLevel.Error, new LogLevelInfo() {consoleColor = ConsoleColor.Red, prefix = "ERROR"}},         // [ERROR]
            {LogLevel.Critical, new LogLevelInfo() {consoleColor = ConsoleColor.DarkRed, prefix = "CRIT "}}   // [CRIT ]
        };
        public SimpleConsoleLogger(string categoryName, Func<string, LogLevel, bool> filter)
        {
            _categoryName = categoryName;
            _filter = filter;

            var r = new Regex(@"\.?.+\.(.+)$");
            var mc = r.Matches(_categoryName);
            _parsedCategoryName = mc[0].Groups[1].Value;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, String> formatter)
        {
            if (!IsEnabled(logLevel)) return;
            if (formatter == null) throw new ArgumentNullException(nameof(formatter));

            var msg = formatter(state, exception);
            if (string.IsNullOrEmpty(msg)) return;
            var newCatName = _categoryName;
            msg = $"[{_dataMapping[logLevel].prefix}][{DateTime.Now:hh:mm:ss}][{_parsedCategoryName}]({eventId.Id}): {msg}";
            if (exception != null)
            {
                msg += Environment.NewLine + Environment.NewLine + exception.ToString();
            }
            Console.ForegroundColor = _dataMapping[logLevel].consoleColor;
            Console.WriteLine(msg);
            Console.ResetColor();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return (_filter == null || _filter(_categoryName, logLevel));
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException("I probably won't bother implementing this.");
        }
    }
}
